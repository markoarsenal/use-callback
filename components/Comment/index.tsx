export type CommentProps = {
  id: number;
  name: string;
  email: string;
  body: string;
  className?: string;
  onClick?: () => void;
};

const Comment = ({ name, email, body, className, onClick }: CommentProps) => {
  return (
    <article className={className} onClick={onClick}>
      <h3 className="font-semibold">{name}</h3>
      <h4 className="text-gray-500 italic">{email}</h4>
      <p>{body}</p>
    </article>
  );
};

export default Comment;
