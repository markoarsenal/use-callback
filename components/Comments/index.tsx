import { memo } from "react";
import Comment, { CommentProps } from "../Comment";

export type CommentsProps = {
  comments: CommentProps[];
  onClick?: () => void;
};

const Comments = ({ comments, onClick }: CommentsProps) => {
  return (
    <section>
      {comments.map((comment) => {
        return (
          <Comment
            {...comment}
            className="mb-4"
            onClick={onClick}
            key={comment.id}
          />
        );
      })}
    </section>
  );
};

export default memo(Comments);
