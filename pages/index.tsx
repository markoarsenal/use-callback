import { Profiler, useCallback, useState } from "react";
import Comment from "../components/Comment";
import Comments from "../components/Comments";
import allComments from "../data/index.json";

// const comments = allComments.slice(0, 100);
const comments = allComments;

const Home = () => {
  const [counter, setCounter] = useState(0);
  const onClick = useCallback(() => console.log("click"), []);

  return (
    <Profiler
      id="Home page"
      onRender={(compName, mode, actualTime, baseTime) =>
        console.log(compName, mode, actualTime, baseTime)
      }
    >
      <main className="max-w-5xl p-8 m-auto">
        <div className="flex justify-center mb-8">
          <button
            onClick={() => setCounter(counter + 1)}
            className="px-3 py-1 border border-gray-500"
          >
            Update {counter}
          </button>
        </div>
        <Comments comments={comments} onClick={onClick} />
        {/* <Comment {...comments[0]} /> */}
      </main>
    </Profiler>
  );
};

export default Home;
